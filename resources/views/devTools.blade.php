<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="csrf-token" content="{{ csrf_token() }}">

   <link rel="stylesheet" href="{{ asset('css/bulma.min.css') }}">

   <title>DevTools</title>

   <style>
      html, body, .select select, .column, .table { background-color: whitesmoke; }

      #dbTables {
         height: 400px;
         border-style: groove;
         border-radius: .375em;
         margin-bottom: 10px;
      }

      #dbTables option:hover { background-color: #e0e9f5; }

      table {
         border-radius: 4px;
         margin: 0 auto;
         display: block;
         overflow-x: auto;
         height: auto;
         max-height: 400px;
         width: auto !important;
         font-size: 14px !important;
      }

      tbody { white-space: nowrap; }

      tbody tr:hover { background-color: #e0e9f5; }

      tbody td {
         padding: 2px !important;
         vertical-align: middle;
      }

      tbody td.select select {
         border-radius: 0;
         border-color: transparent !important;
         border-width: 0;
         width: 100%;
      }

      th {
         background: #e0e9f5;
         position: sticky;
         top: 0;
         vertical-align: bottom;
      }

      th, td {
         border: 1px solid #e3e3e3 !important;
         border-collapse: collapse !important;
      }

      tfoot { display: table-header-group; }

      tfoot tr {
         position: sticky;
         bottom: 0;
      }

      tfoot tr td { background: #e0e9f5; }

      #model, #migration, #controller {
         height: auto;
         max-height: 225px;
         width: 100%;
         overflow: auto;
         border: 1px solid #e3e3e3;
         border-radius: 5px;
         font-family: 'Consolas', 'Courier New', Courier, monospace;
         font-size: 12px;
         white-space: pre;
         padding: 10px;
      }
   </style>

</head>

<body>
   <div class="container">

      <div class="columns mt-4">
         <h4 class="column is-2">Tables</h4>
         <h4 class="column is-2">Validation rules</h4>
      </div>

      <div class="columns">
         @php echo $tables; @endphp
         <div class="column">
            <table id="struc" class="table">
               <thead>
                  <tr>
                     <th style="width:25%">Field</th>
                     <th style="width:25%">Type</th>
                     <th>Null</th>
                     <th>Validation rule</th>
                     <th>Values</th>
                  </tr>
               </thead>
               <tbody></tbody>
               <tfoot>
                  <tr>
                     <td colspan="5">Number of columns: <strong class="ml-3">0</strong></td>
                  </tr>
               </tfoot>
            </table>
         </div>
      </div>

      <div class="columns">
         <input type="checkbox" id="chk1"><label for="chk1" class="ml-3">Generate model class</label>
      </div>
      <div class="columns">
         <p id="model"></p>
      </div>

      <div class="columns">
         <input type="checkbox" id="chk3"><label for="chk3" class="ml-3">Generate controller class</label>
      </div>
      <div class="columns">
         <p id="controller"></p>
      </div>

      <div class="columns">
         <input type="checkbox" id="chk2"><label for="chk2" class="ml-3">Generate migration file</label>
      </div>
      <div class="columns">
         <p id="migration"></p>
      </div>

      <div class="columns mt-3">
         <button class="button is-outline is-primary is-fullwidth column is-2" id="saveBtn">Save code</button>
         <button class="button is-outline is-secondary is-fullwidth column is-2 ml-3 mb-5" id="cancelBtn">Cancel</button>
      </div>

   </div>

   <script>
      (function() {

         "use strict";

         var tableName = '';
         var headers = [];
         var csrf_token = document.querySelector('meta[name="csrf-token"]').content;

         // Bind event listener to dropdown
         document.getElementById("dbTables").addEventListener('change', (event) => {
            tableName = event.target.value;

            // Reset checkboxes and paragraphs.
            document.getElementById('chk1').checked = false;
            document.getElementById('model').textContent = '';

            document.getElementById('chk2').checked = false;
            document.getElementById('migration').textContent = '';

            document.getElementById('chk3').checked = false;
            document.getElementById('controller').textContent = '';

            fetch('getTableStructure', {
               method: 'POST',
               headers: {
                  'Content-Type': 'application/json'
               },
               body: JSON.stringify({
                  _token: csrf_token,
                  tableName: tableName
               }),
            })
            .then((response) => response.json())
            .then(rows => {
               var tbody = document.getElementById("struc").tBodies[0];
               tbody.innerHTML = '';
               var fields = '';
               for (var i = 0; i < rows.length; i++) {
                  // Create the columns for a new row.
                  fields = `<td>${rows[i].COLUMN_NAME}</td>
                            <td>${rows[i].COLUMN_TYPE}</td>
                            <td>${rows[i].IS_NULLABLE}</td>
                            <td class="select" width="100%">
                               <select>
                                  <option value="0">Select a rule</option>
                                  ${['varchar', 'char', 'text', 'longtext'].includes(rows[i].DATA_TYPE) ? '<option value="in">in(["str1", ..., "str-n"])</option>' : ''}
                                  ${['varchar', 'char', 'text', 'longtext'].includes(rows[i].DATA_TYPE) ? '<option value="notIn">notIn(["str1", ..., "str-n"])</option>' : ''}
                                  ${['tinyint','smallint','mediumint','int','bigint'].includes(rows[i].DATA_TYPE) ? '<option value="gt">Greater than</option>' : ''}
                                  ${['tinyint','smallint','mediumint','int','bigint'].includes(rows[i].DATA_TYPE) ? '<option value="get">Greater or equal than</option>' : ''}
                                  ${['tinyint','smallint','mediumint','int','bigint'].includes(rows[i].DATA_TYPE) ? '<option value="lt">Lower than</option>' : ''}
                                  ${['tinyint','smallint','mediumint','int','bigint'].includes(rows[i].DATA_TYPE) ? '<option value="let">Lower or equal than</option>' : ''}
                                  ${['tinyint','smallint','mediumint','int','bigint'].includes(rows[i].DATA_TYPE) ? '<option value="bet">Between</option>' : ''}
                                  ${['varchar', 'char', 'text', 'longtext'].includes(rows[i].DATA_TYPE) ? '<option value="len">Min length</option>' : ''}
                                  ${['varchar', 'char', 'text', 'longtext'].includes(rows[i].DATA_TYPE) ? '<option value="ema">email</option>' : ''}
                               </select>
                            </td>
                            <td contenteditable="true" style="width:20%"></td>`;

                  // Create a row.
                  if (rows[i].Key == 'PRI') {
                     fields = '<tr class="primarykey">' + fields + '</tr>';
                  } 
                  else {
                     if (rows[i].Key == 'MUL') {
                        fields = '<tr class="foreingkey">' + fields + '</tr>';
                     } 
                     else {
                        fields = '<tr>' + fields + '</tr>';
                     }
                  }
                  // Add the new row.
                  tbody.innerHTML += fields;
               };
               document.getElementById("struc").tFoot.rows[0].cells[0].querySelector('strong').innerHTML = rows.length;
            })
            .catch((error) => {
               console.error('Error:', error);
            });
         });


         // Generate model class
         document.getElementById('chk1').addEventListener('click', (event) => {
            var createModel = document.getElementById('chk1').checked;

            if (tableName !== '' && createModel) {
               var rules = [];

               // Get rows of table body.
               var rows = document.getElementById("struc").tBodies[0].rows;

               // Loop through the table rows
               for (var i=0; i < rows.length; i++) {
                  let row = rows[i];
                  let select = row.cells[3].getElementsByTagName('select')[0];   // get the dropdown.
                  let condition = select.options[select.selectedIndex].value;    // get the dropdown selected value.

                  if (condition !== '0') {
                     // build expression to evaluate
                     let obj = {};
                     obj.field = row.cells[0].innerHTML;   // get column name.
                     obj.value = row.cells[4].innerHTML;   // get the comparison values.
                     obj.condition = condition;
                     rules.push(obj);
                  }
               }

               fetch('buildModel', {
                     method: 'POST',
                     headers: {
                        'Content-Type': 'application/json'
                     },
                     body: JSON.stringify({
                        _token: csrf_token,
                        tableName: tableName,
                        rules: rules
                     }),
               })
               .then((response) => response.json())
               .then((data) => {
                  document.getElementById('model').textContent = data.data;
               })
               .catch((error) => {
                  console.error('Error:', error);
               });
            }
            else {
               document.getElementById('chk1').checked = false;
               document.getElementById('model').textContent = '';
               if (tableName == '') {
                  alert('Please select a table from the list.')
               }
            }
         });


         // Generate migration file
         document.getElementById('chk2').addEventListener('click', (event) => {
            var createMigration = document.getElementById('chk2').checked;

            if (tableName !== '' && createMigration) {
               fetch('buildMigration', {
                  method: 'POST',
                  headers: {
                     'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                     _token: csrf_token,
                     tableName: tableName
                  }),
               })
               .then((response) => response.json())
               .then((data) => {
                  document.getElementById('migration').textContent = data.data;
               })
               .catch((error) => {
                  console.error('Error:', error);
               });
            }
            else {
               document.getElementById('chk2').checked = false;
               document.getElementById('migration').textContent = '';
               if (tableName == '') {
                  alert('Please select a table from the list.')
               }
            }
         });


         // Generate controller class
         document.getElementById('chk3').addEventListener('click', (event) => {
            var createController = document.getElementById('chk3').checked;

            if (tableName !== '' && createController) {
               fetch('buildController', {
                     method: 'POST',
                     headers: {
                        'Content-Type': 'application/json'
                     },
                     body: JSON.stringify({
                        _token: csrf_token,
                        tableName: tableName
                     }),
               })
               .then((response) => response.json())
               .then((data) => {
                  document.getElementById('controller').textContent = data.data;
               })
               .catch((error) => {
                  console.error('Error:', error);
               });
            }
            else {
               document.getElementById('chk3').checked = false;
               document.getElementById('controller').textContent = '';
               if (tableName == '') {
                  alert('Please select a table from the list.')
               }
            }
         });
         

         document.getElementById('saveBtn').addEventListener('click', (event) => {
            var model = document.getElementById("model");
            var migration = document.getElementById("migration");
            var controller = document.getElementById("controller");

            fetch('createFiles', {
               method: 'POST',
               headers: {
                  'Content-Type': 'application/json'
               },
               body: JSON.stringify({
                  _token: csrf_token,
                  tableName: tableName,
                  model: model.innerHTML !== "" ? model.innerHTML : "",
                  migration: migration.innerHTML !== "" ? migration.innerHTML : "",
                  controller: controller.innerHTML !== "" ? controller.innerHTML : ""
               }),
            })
            .then((response) => response.json())
            .then((response) => {
               console.log(response.data);
            })
            .catch((error) => {
               console.error('Error:', error);
            });

         });


         document.getElementById('cancelBtn').addEventListener('click', (event) => {
            window.location = '/';
         });

      })();
   </script>
</body>

</html>