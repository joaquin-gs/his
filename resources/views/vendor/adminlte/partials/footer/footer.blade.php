<footer class="main-footer {{ config('adminlte.layout_footer_classes') }}">
    @yield('footer')
</footer>