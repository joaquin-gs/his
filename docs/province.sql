--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `province`;
CREATE TABLE IF NOT EXISTS `province` (
  `provinceId` int UNSIGNED NOT NULL,
  `provinceNameEn` varchar(50) NOT NULL,
  `provinceNameKh` varchar(50) NOT NULL,
  CONSTRAINT PK_Province PRIMARY KEY (`provinceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinces`
--

INSERT INTO `province` (`provinceId`, `provinceNameEn`, `provinceNameKh`) VALUES
(1, 'Banteay Meanchey', 'បន្ទាយមានជ័យ'),
(2, 'Battambang', 'បាត់ដំបង'),
(3, 'Kampong Cham', 'កំពង់ចាម'),
(4, 'Kampong Chhnang', 'កំពង់ឆ្នាំង'),
(5, 'Kampong Speu', 'កំពង់ស្ពឺ'),
(6, 'Kampong Thom', 'កំពង់ធំ'),
(7, 'Kampot', 'កំពត'),
(8, 'Kandal', 'កណ្ដាល'),
(9, 'Koh Kong', 'កោះកុង'),
(10, 'Kratie', 'ក្រចេះ'),
(11, 'Mondul Kiri', 'មណ្ឌលគិរី'),
(12, 'Phnom Penh', 'ភ្នំពេញ'),
(13, 'Preah Vihear', 'ព្រះវិហារ'),
(14, 'Prey Veng', 'ព្រៃវែង'),
(15, 'Pursat', 'ពោធិ៍សាត់'),
(16, 'Ratanakiri', 'រតនគិរី'),
(17, 'Siemreap', 'សៀមរាប'),
(18, 'Preah Sihanouk', 'ព្រះសីហនុ'),
(19, 'Stung Treng', 'ស្ទឹងត្រែង'),
(20, 'Svay Rieng', 'ស្វាយរៀង'),
(21, 'Takeo', 'តាកែវ'),
(22, 'Oddar Meanchey', 'ឧត្តរមានជ័យ'),
(23, 'Kep', 'កែប'),
(24, 'Pailin', 'ប៉ៃលិន'),
(25, 'Tbong Khmum', 'ត្បូងឃ្មុំ');
