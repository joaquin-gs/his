/*
-- Query: SELECT * FROM mts.users
-- Date: 2021-08-26 10:03
*/
INSERT INTO `` (`id`,`roleID`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`language`,`deleted`) VALUES (1,1,'Joaquin','joaquin@angkorhospital.org',NULL,'$2y$10$RRS2728Voup/8isss8ZFe.zyFBeC68NDY5uXIAMfHE6ZFnvg0hJbW','2NKWk7x8MdWjYmVMm3VOrBEpUqPiuWXoDxkYZfmGXVoaTd1v2VNz2uT3Ggcz','2020-08-10 04:14:51','2020-08-10 04:14:51','EN','N');
INSERT INTO `` (`id`,`roleID`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`language`,`deleted`) VALUES (2,2,'Anatolio','anatolio@angkorhospital.org',NULL,'$2y$10$ZkG3Wo.BN/OUVd7Sc10eBObhUBqbAU0ZRCEgvUed8V4CbzW.fDWs6',NULL,'2020-10-02 08:25:19','2020-10-02 08:25:19','EN','N');
INSERT INTO `` (`id`,`roleID`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`language`,`deleted`) VALUES (3,3,'John','john@angkorhospital.org',NULL,'$2y$10$4wPCOnOA6m2xRGxu5Zbx5utBUdJjc.z.7SZYhqfqY3p3q7ZipKtha',NULL,'2020-10-02 08:27:48','2020-10-02 08:27:48','KH','N');
INSERT INTO `` (`id`,`roleID`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`language`,`deleted`) VALUES (9,4,'Caralampio','caralampio@angkorhospital.org',NULL,'$2y$10$kInrLOu9ouUIx.ASw3vghedicjuu10K3nAC1Ea0Gq4v3RWFRsOx7u',NULL,'2020-11-02 04:08:18','2020-11-02 04:08:18','KH','N');
