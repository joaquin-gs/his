-- -----------------------------------------------------
-- Table `PATIENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PATIENT` ;

CREATE TABLE IF NOT EXISTS `PATIENT` (
  `patientId` VARCHAR(12) NOT NULL,
  `familyNameEn` VARCHAR(30) NOT NULL,
  `firstNameEn` VARCHAR(30) NOT NULL,
  `familyNameKh` VARCHAR(30) NULL,
  `firstNameKh` VARCHAR(30) NULL,
  `gender` CHAR(1) NOT NULL,
  `dob` DATE NOT NULL,
  `bloodGroup` VARCHAR(4) NOT NULL,
  `carerNameKh` VARCHAR(45) NOT NULL,
  `relationship` VARCHAR(12) NOT NULL,
  `provinceId` TINYINT NOT NULL,
  `districtId` SMALLINT NOT NULL,
  `communeId` SMALLINT NOT NULL,
  `villageId` SMALLINT NOT NULL,
  `address` VARCHAR(200) NULL,
  `distance` VARCHAR(20) NULL,
  `phone1` VARCHAR(15) NOT NULL,
  `phone2` VARCHAR(15) NULL,
  `isForeigner` CHAR(1) NOT NULL DEFAULT 'N',
  `isEmployee` CHAR(1) NOT NULL DEFAULT 'N',
  `employeeCardId` VARCHAR(10) NULL,
  `employeeCardExpiry` DATE NULL,
  `hasPoorId` CHAR(1) NOT NULL DEFAULT 'N',
  `poorIdExpiry` DATE NULL,
  `hasHEF` CHAR(1) NOT NULL DEFAULT 'N',
  `HEFExpiry` DATE NULL,
  `thirdPartyPayer` CHAR(1) NOT NULL DEFAULT 'N',
  `insuranceName` VARCHAR(80) NULL,
  `billingCode` CHAR(2) NOT NULL DEFAULT 'A',
  `LMTR` CHAR(2) NULL,
  PRIMARY KEY (`patientId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `REFERRER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `REFERRER` ;

CREATE TABLE IF NOT EXISTS `REFERRER` (
  `referrerId` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `referrerName` VARCHAR(200) NOT NULL,
  `address` VARCHAR(200) NULL,
  `deleted` CHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`referrerId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VISIT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `VISIT` ;

CREATE TABLE IF NOT EXISTS `VISIT` (
  `visitId` INT NOT NULL AUTO_INCREMENT,
  `patientId` VARCHAR(12) NOT NULL,
  `visitDate` DATETIME NOT NULL,
  `operationalDistrict` VARCHAR(20) NULL,
  `isFollowUp` CHAR(1) NOT NULL DEFAULT 'N',
  `isFirstTime` CHAR(1) NOT NULL DEFAULT 'N',
  `referredFrom` SMALLINT UNSIGNED NULL,
  `patientHeight` FLOAT NULL,
  `patientWeight` FLOAT NULL,
  `chiefComplaint` VARCHAR(2000) NOT NULL,
  `registrar` VARCHAR(20) NOT NULL,
  `note` VARCHAR(100) NULL,
  PRIMARY KEY (`visitId`, `patientId`),
  CONSTRAINT `FK_VisitPatient`
    FOREIGN KEY (`patientId`)
    REFERENCES `PATIENT` (`patientId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Fk_visit_referrer`
    FOREIGN KEY (`referredFrom`)
    REFERENCES `REFERRER` (`referrerId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DIAGNOSIS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `DIAGNOSIS` ;

CREATE TABLE IF NOT EXISTS `DIAGNOSIS` (
  `diagnosisId` VARCHAR(10) NOT NULL,
  `diagnosisName` VARCHAR(200) NOT NULL,
  `mohName` VARCHAR(200) NULL,
  `triageUseOnly` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'Indicates if the diagnosis code is used only by the triage unit',
  PRIMARY KEY (`diagnosisId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BED`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BED` ;

CREATE TABLE IF NOT EXISTS `BED` (
  `bedId` TINYINT UNSIGNED NOT NULL,
  `unitId` TINYINT UNSIGNED NOT NULL,
  `bedStatus` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`bedId`, `unitId`)
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UNIT_PATIENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UNIT_PATIENT` ;

CREATE TABLE IF NOT EXISTS `UNIT_PATIENT` (
  `visitId` INT NOT NULL,
  `patientId` VARCHAR(12) NOT NULL,
  `unitId` TINYINT UNSIGNED NOT NULL,
  `arrivalTime` DATETIME NOT NULL COMMENT 'enqueueTime is used only when a patient is just transferred to another ward.',
  `doctor` SMALLINT NULL,
  `nurse` SMALLINT NOT NULL,
  `admisionDx` VARCHAR(10) NULL,
  `bedId` TINYINT UNSIGNED NULL,
  `patientStatus` VARCHAR(10) NOT NULL COMMENT 'patientStatus: Waiting, Imaging/Scanning, Observation, admitted, discharged, deceased, Lab examination, disappeared',
  `dischargeDx` VARCHAR(10) NULL,
  `diagnosisCase` CHAR(3) NULL,
  `dischargeType` SMALLINT NULL,
  `dischargeTime` DATETIME NOT NULL,
  `dischargedBy` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`visitId`, `patientId`, `unitId`),
  CONSTRAINT `Visit_UnitPatient`
    FOREIGN KEY (`visitId` , `patientId`)
    REFERENCES `VISIT` (`visitId` , `patientId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Diagnosis_UnitPatient`
    FOREIGN KEY (`dischargeDx`)
    REFERENCES `DIAGNOSIS` (`diagnosisId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Bed_UnitPatient`
    FOREIGN KEY (`bedId` , `unitId`)
    REFERENCES `BED` (`bedId` , `unitId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION   
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ALLERGY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ALLERGY` ;

CREATE TABLE IF NOT EXISTS `ALLERGY` (
  `patientId` VARCHAR(12) NOT NULL,
  `drugId` CHAR(5) NOT NULL,
  `dose` VARCHAR(45) NOT NULL,
  `reactionType` VARCHAR(45) NULL,
  `note` VARCHAR(200) NULL,
  PRIMARY KEY (`patientId`, `drugId`),
  CONSTRAINT `Fk_PatientAllergy`
    FOREIGN KEY (`patientId`)
    REFERENCES `PATIENT` (`patientId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PRESCRIPTION`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PRESCRIPTION` ;

CREATE TABLE IF NOT EXISTS `PRESCRIPTION` (
  `prescriptionId` INT NOT NULL AUTO_INCREMENT,
  `visitId` INT NOT NULL,
  `patientId` VARCHAR(12) NOT NULL,
  `unitId` TINYINT UNSIGNED NOT NULL,
  `prescribedOn` DATETIME NOT NULL,
  `prescribedBy` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`prescriptionId`),
  CONSTRAINT `UnitPatient_Prescription`
    FOREIGN KEY (`visitId` , `patientId` , `unitId`)
    REFERENCES `UNIT_PATIENT` (`visitId` , `patientId` , `unitId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VITAL`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `VITAL` ;

CREATE TABLE IF NOT EXISTS `VITAL` (
  `visitId` INT NOT NULL,
  `patientId` VARCHAR(12) NOT NULL,
  `unitId` TINYINT UNSIGNED NOT NULL,
  `vitalTime` DATETIME NOT NULL,
  `temperature` FLOAT NULL,
  `systolicBloodPressure` TINYINT NULL,
  `diastolicBloodPressure` TINYINT NULL,
  `heartRate` TINYINT NULL,
  `respiratoryRate` TINYINT NULL,
  `oxigenSaturation` TINYINT NULL,
  PRIMARY KEY (`visitId`, `patientId`, `unitId`, `vitalTime`),
  CONSTRAINT `Vital_UnitPatient`
    FOREIGN KEY (`visitId`, `patientId`, `unitId`)
    REFERENCES `UNIT_PATIENT` (`visitId`, `patientId`, `unitId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PRESCRIPTION_DETAIL`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PRESCRIPTION_DETAIL` ;

CREATE TABLE IF NOT EXISTS `PRESCRIPTION_DETAIL` (
  `prescriptionId` INT NOT NULL,
  `drugId` CHAR(5) NOT NULL,
  `drugQuantity` TINYINT NOT NULL,
  `dosage` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`prescriptionId`, `drugId`),
  CONSTRAINT `FK_Prescription_DetailPrescription`
    FOREIGN KEY (`prescriptionId`)
    REFERENCES `PRESCRIPTION` (`prescriptionId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `IMAGING`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMAGING` ;

CREATE TABLE IF NOT EXISTS `IMAGING` (
  `visitId` INT NOT NULL,
  `patientId` VARCHAR(12) NOT NULL,
  `unitId` TINYINT UNSIGNED NOT NULL,
  `requestTime` DATETIME NULL COMMENT 'The date/time an image request is received.',
  `receptionTime` DATETIME NULL COMMENT 'The date/time a patient is attended.',
  `ultraSoundType` VARCHAR(30) NULL,
  `radiologist` VARCHAR(20) NULL,
  `radiologyType` VARCHAR(45) NULL,
  `radiologyView` VARCHAR(20) NULL,
  `note` VARCHAR(200) NULL,
  PRIMARY KEY (`visitId`, `patientId`, `unitId`),
  CONSTRAINT `Imaging_UnitPatient`
    FOREIGN KEY (`visitId`, `patientId`, `unitId`)
    REFERENCES `UNIT_PATIENT` (`visitId`, `patientId`, `unitId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `APPOINTMENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `APPOINTMENT` ;

CREATE TABLE IF NOT EXISTS `APPOINTMENT` (
  `appointmentId` INT NOT NULL AUTO_INCREMENT,
  `visitId` INT NOT NULL,
  `patientId` VARCHAR(12) NOT NULL,
  `unitId` TINYINT UNSIGNED NOT NULL,
  `appointmentDate` DATETIME NOT NULL,
  `doctor` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`appointmentId`),
  CONSTRAINT `UnitPatient_Appointment`
    FOREIGN KEY (`visitId`, `patientId`, `unitId`)
    REFERENCES `UNIT_PATIENT` (`visitId`, `patientId`, `unitId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

