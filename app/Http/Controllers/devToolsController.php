<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class devToolsController {
   
   private array $tables;

   public function index() {
      $strTables = $this->getDBTables();
      return view('devTools', ['tables' => $strTables]);
   }


   private function getDBTables() {
      $this->tables = DB::select('SHOW TABLES');
      $dropDown = '<select id="dbTables" size="10" class="column is-2">';
      foreach($this->tables as $table) {
         $dropDown .= '<option value="'.$table->Tables_in_emr.'">'. $table->Tables_in_emr . '</option>';
      }
      $dropDown .= '</select>';
      return $dropDown;
   }


   public function getTableStructure(Request $request) {
      $tableName = $request['tableName'];
      $dbName = env('DB_DATABASE');
      
      // Retrieve table structure from schema.
      $query = "SELECT COLUMN_NAME, 
                       column_default, 
                       IS_NULLABLE, 
                       DATA_TYPE, 
                       COLUMN_TYPE,
                       CHARACTER_MAXIMUM_LENGTH, 
                       NUMERIC_PRECISION, 
                       numeric_scale, 
                       column_key, 
                       EXTRA
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = ?
                  AND table_name = ?
                ORDER BY ordinal_position";
      $columns = DB::select($query, [$dbName, $tableName]);
      return $columns; 
   }
   
   
   public function buildModel(Request $request) {
      $tableName = $request['tableName'];
      $aRules = $request['rules'];

      $condition = '';
      $phpDataType = '';  // string, integer, float, boolean

      $req = new Request();
      $req->setMethod('POST');
      $req->request->add(['tableName' => $tableName]);
      $cols = $this->getTableStructure($req);

      // Create PHP model class file
      $content = "<?php" . PHP_EOL . PHP_EOL . 'namespace App\Models;' . PHP_EOL . PHP_EOL;
      $content .= "use App\Models\baseModel;" . PHP_EOL . PHP_EOL;
      $content .= "class " . $tableName . " extends baseModel" . PHP_EOL;
      $content .= "{" . PHP_EOL;

      $attributes = '';
      $getAndSet = '';
      $rule = array();
      $hasComment = false;
      $aColumns = array_column($aRules, "field");

      $numKeyFields = 0;
      $keyCols = '';
      // Loop through table rows to generate the Model Class code.
      for ($i=0; $i < count($cols); $i++) {
         $key = array_search($cols[$i]->COLUMN_NAME, $aColumns);
         if ($key !== false) {
            // Get the validation rule that applies to that specific column.
            $rule = $aRules[$key];
         }
         
         // Map MySQL data types to PHP types.
         switch ($cols[$i]->DATA_TYPE) {
            case in_array($cols[$i]->DATA_TYPE, ['tinyint', 'smallint', 'mediumint', 'int', 'bigint']):
               $phpDataType = 'int';
               break;
            case in_array($cols[$i]->DATA_TYPE, ['varchar', 'char', 'text', 'longtext']):
               $phpDataType = 'string';
               break;
            case in_array($cols[$i]->DATA_TYPE, ['float', 'double']):
               $phpDataType = 'float';
               break;
            case in_array($cols[$i]->DATA_TYPE, ['datetime', 'timestamp', 'date']):
               $phpDataType = 'string';
               break;
            case 'bit':
               $phpDataType = 'boolean';
         }

         // Write the list of attributes.
         if (in_array($cols[$i]->COLUMN_KEY, ['PRI', 'MUL'])) {
            $numKeyFields++;
            if ($numKeyFields > 1) {
               $keyCols .= '[\'' . $cols[$i]->COLUMN_NAME . ', \'';
            }
            else {
               $keyCols = $cols[$i]->COLUMN_NAME;
            }
            continue;
         }
         else {
            if ($keyCols !== '') {
               $attributes .= space(3) . 'protected $primaryKey = \'' . $keyCols . ($numKeyFields > 1 ? '\'];' : '\'') . PHP_EOL . PHP_EOL;
               $keyCols = '';
            }
         }
         $attributes .= space(3) . 'protected ' . $phpDataType . ' ' . $cols[$i]->COLUMN_NAME . ';' . PHP_EOL;

         if (!$hasComment) {
            $getAndSet .= space(3). '// ------------------------------'. PHP_EOL;
            $getAndSet .= space(3). '// Properties getters and setters '. PHP_EOL;
            $getAndSet .= space(3). '// ------------------------------'. PHP_EOL . PHP_EOL;
            $hasComment = true;
         }

         // Generate the getter method for the attribute.
         $getAndSet .= space(3) . 'protected function get' . ucfirst($cols[$i]->COLUMN_NAME) . '():' . $phpDataType . ' { return $this->' . $cols[$i]->COLUMN_NAME . '; }' . PHP_EOL;

         // Generate the setter method for the attribute.
         if ($cols[$i]->EXTRA !== 'auto_increment') {
            $getAndSet .= space(3) . 'protected function set' . ucfirst($cols[$i]->COLUMN_NAME) . '(' . $phpDataType . ' $value) {' . PHP_EOL;

            // If data type is a string...
            if (in_array($cols[$i]->DATA_TYPE, ['varchar', 'char', 'text', 'longtext'])) {
               $getAndSet .= space(6) . '$error = \'\';' . PHP_EOL;
               $getAndSet .= space(6) . '$len = strlen($value);' . PHP_EOL;

               // If the column is not null...
               if ($cols[$i]->IS_NULLABLE == 'NO') {
                  $getAndSet .= space(6) . 'if ($len == 0) {' . PHP_EOL;
                  $getAndSet .= space(9) . '$error = self::FIELD_REQUIRED;' . PHP_EOL;
                  $getAndSet .= space(6) . '}' . PHP_EOL;
                  $getAndSet .= space(6) . 'else {' . PHP_EOL;
                  $getAndSet .= space(9) . 'if ($len <= ' . $cols[$i]->CHARACTER_MAXIMUM_LENGTH . ') {' . PHP_EOL;

                  // If there is a user validation rule...
                  if (count($rule) > 0) {
                     if (in_array($rule['condition'], ['in', 'notIn'])) {
                        $aContent = explode(',', trim($rule['value']));
                        $list = '';
                        foreach ($aContent as $index => $value) {
                           $list .= "'" . $value . "'" . ',';
                        }
                        $list = substr($list, 0, strlen($list) - 1);
                        $condition = 'in_array($value, [' . $list . '])';
                     }
                     if ($rule['condition'] == 'len') {
                        $condition = 'strlen(' . $value . ') <= ' . $rule['value'];
                     }
                     if ($rule['condition'] == 'ema') {
                        $condition = 'filter_var("' . $cols[$i]->COLUMN_NAME . '", FILTER_VALIDATE_EMAIL)';
                     }

                     // Add validation rule text.
                     $getAndSet .= space(12) . '// Validation rule' . PHP_EOL;
                     $getAndSet .= space(12) . 'if (!' . $condition . ') {' . PHP_EOL;
                     $getAndSet .= space(15) . '$error = self::' . ($rule['condition'] == 'ema' ? 'INVALID_EMAIL;' : 'INVALID_VALUE;') . PHP_EOL;
                     $getAndSet .= space(12) . '}' . PHP_EOL;
                     $getAndSet .= space(12) . 'else {' . PHP_EOL;
                  }

                  $getAndSet .= (count($rule) > 0 ? space(15): space(12)) . '$this->' . $cols[$i]->COLUMN_NAME . ' = $value; ' . PHP_EOL;
                  if (count($rule) > 0) {
                     $getAndSet .= space(12) . '}' . PHP_EOL;
                  }
                  $getAndSet .= space(9) . '}' . PHP_EOL;
                  $getAndSet .= space(9) . 'else {' . PHP_EOL;
                  $getAndSet .= space(12) . '$error = self::FIELD_TOO_LONG;' . PHP_EOL;
                  $getAndSet .= space(9) . '}' . PHP_EOL;
                  $getAndSet .= space(6) . '}' . PHP_EOL;
               }
               else {
                  $getAndSet .= space(6) . 'if ($len > 0) {' . PHP_EOL;
                  $getAndSet .= space(9) . 'if ($len <= ' . $cols[$i]->CHARACTER_MAXIMUM_LENGTH . ') {' . PHP_EOL;
                  $getAndSet .= space(12) . '$this->' . $cols[$i]->COLUMN_NAME . ' = $value; ' . PHP_EOL;
                  $getAndSet .= space(9) . '}' . PHP_EOL;
                  $getAndSet .= space(9) . 'else {' . PHP_EOL;
                  $getAndSet .= space(12) . '$error = self::FIELD_TOO_LONG;' . PHP_EOL;
                  $getAndSet .= space(9) . '}' . PHP_EOL;
                  $getAndSet .= space(6) . '}' . PHP_EOL;
               }
               $getAndSet .= space(6) . 'if ($error !== \'\') {' . PHP_EOL;
               $getAndSet .= space(9) . '$this->addError(' . $cols[$i]->COLUMN_NAME . ', $error);' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
            }

            // If column is a date type...
            if (in_array($cols[$i]->DATA_TYPE, ['datetime', 'timestamp', 'date'])) {
               $getAndSet .= space(6) . '$error = \'\';' . PHP_EOL;
               $getAndSet .= space(6) . '$date = $this->dateToMySQL($value);' . PHP_EOL;
               $getAndSet .= space(6) . 'if (strpos($date, "Date") === false) {' . PHP_EOL;
               $getAndSet .= space(9) . '$this->' . $cols[$i]->COLUMN_NAME . ' = $value;' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
               $getAndSet .= space(6) . 'else {' . PHP_EOL;
               $getAndSet .= space(9) . '$error = $date;' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
               $getAndSet .= space(6) . 'if ($error !== \'\') {' . PHP_EOL;
               $getAndSet .= space(9) . '$this->addError(' . $cols[$i]->COLUMN_NAME . ', $error);' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
            }

            // If column is time type...
            if ($cols[$i]->DATA_TYPE == 'time') {
               $getAndSet .= space(6) . '$error = \'\';' . PHP_EOL;

               if ($cols[$i]->IS_NULLABLE == 'NO') {
                  $getAndSet .= space(6) . '$len = strlen($value);' . PHP_EOL;
                  $getAndSet .= space(6) . 'if ($len == 0) {' . PHP_EOL;
                  $getAndSet .= space(9) . '$error = self::FIELD_REQUIRED;' . PHP_EOL;
                  $getAndSet .= space(6) . '}' . PHP_EOL;
                  $getAndSet .= space(6) . 'else {' . PHP_EOL;
                  $getAndSet .= space(9) . 'if ($len <= ' . $cols[$i]->CHARACTER_MAXIMUM_LENGTH . ') {' . PHP_EOL;
                  $getAndSet .= space(12) . '$this->' . $cols[$i]->COLUMN_NAME . ' = $value; ' . PHP_EOL;
                  $getAndSet .= space(9) . '}' . PHP_EOL;
                  $getAndSet .= space(9) . 'else {' . PHP_EOL;
                  $getAndSet .= space(12) . '$error = self::FIELD_TOO_LONG;' . PHP_EOL;
                  $getAndSet .= space(9) . '}' . PHP_EOL;
                  $getAndSet .= space(6) . '}' . PHP_EOL;
               }
               $getAndSet .= space(6) . 'if ($error !== \'\') {' . PHP_EOL;
               $getAndSet .= space(9) . '$this->addError(' . $cols[$i]->COLUMN_NAME . ', $error);' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
            }

            // If data type is integer...
            if (in_array($cols[$i]->DATA_TYPE, ['tinyint', 'smallint', 'mediumint', 'int', 'bigint'])) {
               $getAndSet .= space(6) . '$error = \'\';' . PHP_EOL;
               switch ($cols[$i]->DATA_TYPE) {
                  case 'tinyint':
                     $condition = str_contains($cols[$i]->COLUMN_TYPE, 'unsigned') ? '$value < 0 || $value > 255' : '$value < -128 || $value > 127';
                     break;

                  case 'smallint':
                     $condition = str_contains($cols[$i]->COLUMN_TYPE, 'unsigned') ? '$value < 0 || $value > 65535' : '$value < -32768 || $value > 32767';
                     break;
                  
                  case 'mediumint':
                     $condition = str_contains($cols[$i]->COLUMN_TYPE, 'unsigned') ? '$value < 0 || $value > 16777215' : '$value < -8388608 || $value > 8388607';
                     break;

                  case 'int':
                     $condition = str_contains($cols[$i]->COLUMN_TYPE, 'unsigned') ? '$value < 0 || $value > 4294967295' : '$value < -2147483648 || $value > 2147483647';
                     break;
                  
                  case 'bigint':
                     $condition = str_contains($cols[$i]->COLUMN_TYPE, 'unsigned') ? '$value < 0 || $value > 18446744073709551615' : '$value < -2147483648 || $value > 9223372036854775807';
               }
               $getAndSet .= space(6) . 'if (' . $condition . ') {' . PHP_EOL;
               $getAndSet .= space(9) . '$error = self::FIELD_OUT_OF_RANGE;' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
               $getAndSet .= space(6) . 'else {' . PHP_EOL;

               // If there is a user validation rule...
               if (count($rule) > 0) {
                  $operator = '';
                  if ($rule['condition'] == 'bet') {
                     $condition = '(' . $cols[$i]->COLUMN_NAME . ' <= ' . $rule['value'] . ') and (' . $cols[$i]->COLUMN_NAME . ' >= ' . $rule['value'] . ')';
                  }
                  else {
                     if ($rule['condition'] == 'gt') { $operator = ' > '; }
                     if ($rule['condition'] == 'get') { $operator = ' >= '; }
                     if ($rule['condition'] == 'lt') { $operator = ' < '; }
                     if ($rule['condition'] == 'let') { $operator = ' <= '; }
                     $condition = $cols[$i]->COLUMN_NAME . $operator . $rule['value'];
                  }

                  // Add validation rule text.
                  $getAndSet .= space(6) . '// Validation rule' . PHP_EOL;
                  $getAndSet .= space(6) . 'if (!' . $condition . ') {' . PHP_EOL;
                  $getAndSet .= space(9) . '$error = self::INVALID_VALUE;' . PHP_EOL;
                  $getAndSet .= space(6) . '}' . PHP_EOL;
               }

               $getAndSet .= space(9) . '$this->' . $cols[$i]->COLUMN_NAME . ' = $value;' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
            }

            // If data type is Blob
            if (in_array($cols[$i]->DATA_TYPE, ['tinyblob', 'blob', 'mediumblob', 'longblob', 'tinytext', 'text', 'mediumtext', 'longtext'])) {
               $getAndSet .= space(6) . '$value = $_FILES[0]["size"];' . PHP_EOL;
               switch ($cols[$i]->DATA_TYPE) {
                  case in_array($cols[$i]->DATA_TYPE, ['tinyblob', 'tinytext']):
                     $condition = '$value > 255';
                     break;
                  case in_array($cols[$i]->DATA_TYPE, ['blob', 'text']):
                     $condition = '$value > 65535';
                     break;
                  case in_array($cols[$i]->DATA_TYPE, ['mediumblob', 'mediumtext']):
                     $condition = '$value > 16777215';
                     break;
                  case in_array($cols[$i]->DATA_TYPE, ['longblob', 'longtext']):
                     $condition = '$value > 4294967295';
               }
               $getAndSet .= space(6) . 'if (' . $condition . ') {' . PHP_EOL;
               $getAndSet .= space(9) . '$error = self::FIELD_TOO_LONG;' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
               $getAndSet .= space(6) . 'else {' . PHP_EOL;
               $getAndSet .= space(9) . '$this->' . $cols[$i]->COLUMN_NAME . ' = $value;' . PHP_EOL;
               $getAndSet .= space(6) . '}' . PHP_EOL;
            }

            $getAndSet .= space(3) . '}' . PHP_EOL . PHP_EOL;
         }
         else {
            $getAndSet .= PHP_EOL;
         }
      }

      $content .= $attributes . PHP_EOL;
      $content .= $getAndSet;
      $content .= space(3) . '// ------------------------' . PHP_EOL;
      $content .= space(3) . '// Basic controller actions' . PHP_EOL;
      $content .= space(3) . '// ------------------------' . PHP_EOL . PHP_EOL;
      
      $content .= space(3) . '/**' . PHP_EOL;
      $content .= space(3) . '* Saves the data into table ' . $tableName . PHP_EOL;
      $content .= space(3) . '*' . PHP_EOL;
      $content .= space(3) . '* @param array $fields' . PHP_EOL;
      $content .= space(3) . '* @return void' . PHP_EOL;
      $content .= space(3) . '*/' . PHP_EOL;
      $content .= space(3) . 'protected function store($fields) {' . PHP_EOL;
      $content .= space(6) . '// Your code...' . PHP_EOL;         
      $content .= space(3) . '}' . PHP_EOL . PHP_EOL;

      $content .= space(3) . '/**' . PHP_EOL;
      $content .= space(3) . '* Marks the row, which has the $key, as deleted.' . PHP_EOL;
      $content .= space(3) . '* ' . PHP_EOL;
      $content .= space(3) . '* @param mixed $key' . PHP_EOL;
      $content .= space(3) . '*/' . PHP_EOL;
      $content .= space(3) . 'public function delete($key) {' . PHP_EOL;
      $content .= space(6) . '// Your code...' . PHP_EOL;
      $content .= space(3) . '}' . PHP_EOL . PHP_EOL;
      
      $content .= '}' . PHP_EOL;
      return response()->json(["data" => $content]);
   }


   public function buildMigration(Request $request) {
      $tableName = $request['tableName'];
      $columns = '';

      $req = new Request();
      $req->setMethod('POST');
      $req->request->add(['tableName' => $tableName]);
      $struc = $this->getTableStructure($req);

      foreach ($struc as $cols) {
         $isUnsigned = str_contains($cols->COLUMN_TYPE, 'unsigned');
         $precision = $cols->NUMERIC_PRECISION;
         $scale = $cols->NUMERIC_SCALE;
         $strLen = $cols->CHARACTER_MAXIMUM_LENGTH;
         $required = $cols->IS_NULLABLE === 'NO';

         $blobs = '';
         $columns .= space(9);
         switch ($cols->DATA_TYPE) {
            case 'date':
               $columns .= '$table->date("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'datetime':
               $columns .= '$table->dateTime("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'timestamp':
               $columns .= '$table->timestamp("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'time':
               $columns .= '$table->time("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'char':
               $columns .= '$table->char("' . $cols->COLUMN_NAME . '", ' . $strLen . ')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'string':
            case 'varchar':
               $columns .= '$table->string("' . $cols->COLUMN_NAME . '", ' . $strLen . ')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'tinytext':
               $columns .= '$table->tinyText("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'text':
               $columns .= '$table->text("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'mediumtext':
               $columns .= '$table->mediumText("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'longtext':
               $columns .= '$table->longText("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'tinyint':
               $columns .= ($isUnsigned ? '$table->unsignedTinyInteger("' : '$table->tinyInteger("') . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'smallint':
               $columns .= ($isUnsigned ? '$table->unsignedSmallInteger(\'' : '$table->smallInteger(\'') . $cols->COLUMN_NAME . '\')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'mediumint':
               $columns .= ($isUnsigned ? '$table->unsignedMediumInteger(\'' : '$table->mediumInteger(\'') . $cols->COLUMN_NAME . '\')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'int':
               $columns .= ($isUnsigned ? '$table->unsignedInteger(\'' : '$table->integer(\'') . $cols->COLUMN_NAME . '\')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'bigint':
               $columns .= ($isUnsigned ? '$table->unsignedBigInteger(\'' : '$table->bigInteger(\'') . $cols->COLUMN_NAME . '\')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'float':
               $columns .= '$table->float("' . $cols->COLUMN_NAME . '")' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'double':
               $columns .= '$table->double("' . $cols->COLUMN_NAME . '",' . $precision . ',' . $scale . ')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case 'decimal':
               $columns .= '$table->decimal("' . $cols->COLUMN_NAME . '",' . $precision . ',' . $scale . ')' . ($required ? ';' : '->nullable();') . PHP_EOL;
               break;
            case in_array($cols->DATA_TYPE, ['tinyblob', 'blob', 'mediumblob', 'longblob']):
               $blobs .= 'DB::statement("ALTER TABLE ' . $tableName . ' ADD ' . $cols->COLUMN_NAME .' '. $cols->DATA_TYPE . '");' . PHP_EOL;
         }
      }

      // Find the primary key.
      $query = "SELECT COLUMN_NAME
               FROM INFORMATION_SCHEMA.COLUMNS
               WHERE TABLE_SCHEMA = ?
               AND TABLE_NAME = ?
               AND COLUMN_KEY = 'PRI';";
      $pks = DB::select($query, [env('DB_DATABASE'), $tableName]);
      
      if (count($pks) > 0) {
         // Concat all key field names in one string.
         $tkeys = '';
         for ($i=0; $i < count($pks); $i++) {
            $tkeys .= '"' . $pks[$i]->COLUMN_NAME . '",';
         }
         // Get rid of last comma
         $tkeys = substr($tkeys, 0, strlen($tkeys)-1);
         // Add primary key to table.
         $columns .= space(9) . '$table->primary(['. $tkeys . ']);';
      }

      $query = "SELECT COLUMN_NAME,
                  CONSTRAINT_NAME,
                  REFERENCED_TABLE_NAME,
                  REFERENCED_COLUMN_NAME
               FROM information_schema.KEY_COLUMN_USAGE
               WHERE CONSTRAINT_SCHEMA = ? AND
               TABLE_NAME = ? AND 
               REFERENCED_TABLE_SCHEMA IS NOT NULL AND
               REFERENCED_TABLE_NAME IS NOT NULL AND
               REFERENCED_COLUMN_NAME IS NOT NULL;";
      $fks = DB::select($query, [env('DB_DATABASE'), $tableName]);
      if (count($fks) > 0) {
         // There are Foreign keys defined for this table.
         foreach ($fks as $fk) {
            $columns .= space(9) . '$table->foreign("' . $fk->COLUMN_NAME . '", "Fk_' . $fk->REFERENCED_TABLE_NAME . '_' . $tableName . '")->references("' . $fk->REFERENCED_COLUMN_NAME . '")->on("' . $tableName . '");' . PHP_EOL;
         }
      }

      $content = "<?php" . PHP_EOL . PHP_EOL ;
      $content .= "use Illuminate\Database\Migrations\Migration;" . PHP_EOL;
      $content .= "use Illuminate\Database\Schema\Blueprint;" . PHP_EOL;
      $content .= "use Illuminate\Support\Facades\Schema;" . PHP_EOL . PHP_EOL;
      $content .= "class create" . $tableName . "Table extends Migration" . PHP_EOL;
      $content .= "{" . PHP_EOL;
      $content .= "   /**" . PHP_EOL;
      $content .= "    * Run the migrations." . PHP_EOL;
      $content .= "    *" . PHP_EOL;
      $content .= "    * @return void" . PHP_EOL;
      $content .= "    */" . PHP_EOL;
      $content .= "   public function up() {" . PHP_EOL;
      $content .= "      Schema::create('$tableName', function (Blueprint " . '$table' . ") {". PHP_EOL;
      $content .= $columns;
      $content .= "      });". PHP_EOL;
      $content .= "   }". PHP_EOL . PHP_EOL;
      if ($blobs !== '') {
         $content .= "// Create blob columns." . PHP_EOL;
         $content .= $blobs . PHP_EOL . PHP_EOL;
      }
      $content .= "   /**" . PHP_EOL;
      $content .= "    * Reverse the migrations." . PHP_EOL;
      $content .= "    *" . PHP_EOL;
      $content .= "    * @return void" . PHP_EOL;
      $content .= "    */" . PHP_EOL;
      $content .= "   public function down() {" . PHP_EOL;
      $content .= "      Schema::dropIfExists('" . $tableName . "');" . PHP_EOL;
      $content .= "   }". PHP_EOL . PHP_EOL;
      $content .= "}";
      return response()->json(["data"=>$content]);
   }
   

   public function buildController(Request $request) {
      $tableName = $request['tableName'];

      // Begin creating PHP model class file
      $content = "<?php" . PHP_EOL . PHP_EOL . 'namespace App\Http\Controllers;' . PHP_EOL . PHP_EOL;
      $content .= "use Illuminate\Http\Request;" . PHP_EOL;
      $content .= "use App\Models\\$tableName;" . PHP_EOL . PHP_EOL;
      $content .= "class " . $tableName . "Controller extends Controller" . PHP_EOL;
      $content .= "{" . PHP_EOL;
      $content .= space(3) . "// The model to be managed by this controller." . PHP_EOL;
      $content .= space(3) . "private $$tableName;" . PHP_EOL . PHP_EOL;

      $content .= space(3) . "public function __construct() {" . PHP_EOL;
      $content .= space(6) . "// Require user authentication." . PHP_EOL;
      $content .= space(6) . '$this->middleware("auth");' . PHP_EOL;
      $content .= space(6) . "// Instantiate the model." . PHP_EOL;
      $content .= space(6) . '$this->' . $tableName . ' = new ' . $tableName . '();' . PHP_EOL;
      $content .= space(3) . "}" . PHP_EOL . PHP_EOL;
      
      $content .= space(3) . "// Display initial view." . PHP_EOL;
      $content .= space(3) . "public function index() {" . PHP_EOL;
      $content .= space(6) . 'return view("' . $tableName . '");' . PHP_EOL;
      $content .= space(3) . "}" . PHP_EOL . PHP_EOL;

      $content .= space(3) . "// Store a resource in storage." . PHP_EOL;
      $content .= space(3) . 'public function store(Request $request) {' . PHP_EOL;
      $content .= space(6) . '$fields = $request->all();' . PHP_EOL;
      $content .= space(6) . '$this->' . $tableName . '->store($fields);' . PHP_EOL;
      $content .= space(6) . 'return view("' . $tableName . '");' . PHP_EOL;
      $content .= space(3) . "}" . PHP_EOL. PHP_EOL;

      $content .= space(3) . "// Remove the specified resource from storage." . PHP_EOL;
      $content .= space(3) . 'public function delete(Request $request) {' . PHP_EOL;
      $content .= space(6) . "//" . PHP_EOL;
      $content .= space(3) . "}" . PHP_EOL;

      $content .= '}' . PHP_EOL . PHP_EOL;
      return response()->json(["data" => $content]);
   }


   public function createFiles(Request $request) {
      $tableName = $request['tableName'];
      $model = $request['model'];
      $controller = $request['controller'];
      $migration = $request['migration'];

      if ( !empty($model) ) {
         $file = app_path() . '/Models/' . $tableName . '.php';
         $result = file_put_contents($file, html_entity_decode($model));
      }
      if (!empty($controller)) {
         $file = app_path() . '/Http/Controllers/' . $tableName . 'Controller.php';
         $result = file_put_contents($file, html_entity_decode($controller));
      }
      if (!empty($migration)) {
         $file = database_path() . '/migrations/' . date('Y_m_d_His') . '_create_' . $tableName . '_table.php';
         $result = file_put_contents($file, html_entity_decode($migration));
      }
   }
}


function space(int $quantity): string {
   return str_repeat(' ', $quantity);
}
