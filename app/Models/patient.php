<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\baseModel;
use Illuminate\Support\Facades\DB;

class patient extends baseModel {
   use HasFactory;

   public $timestamps = false;
   protected $table = 'patients';
   protected $primaryKey = 'patientID';
   protected $keyType = 'string';
   protected $fillable = ['patientID',
                          'familyNameEn',
                          'firstNameEn',
                          'familyNameKh',
                          'firstNameKh',
                          'gender',
                          'dob',
                          'bloodGroup',
                          'carerNameKh',
                          'relationship',
                          'provinceID',
                          'districtID',
                          'communeID',
                          'villageID',
                          'address',
                          'distance',
                          'phone1',
                          'phone2',
                          'isForeigner',
                          'isEmployee',
                          'employeeCardId',
                          'employeeCardExpiry',
                          'hasPoorId',
                          'poorIdExpiry',
                          'hasHEF',
                          'HEFExpiry',
                          'thirdPartyPayer',
                          'insuranceName',
                          'billingCode',
                          'LMTR',
                          ];

   protected $rules = array(
      'patientID' => 'required|max:12',
      'familyNameEn' => 'required|max:30',
      'firstNameEn' => 'required|max:30',
      'familyNameKh' => 'max:30',
      'firstNameKh' => 'max:30',
      'gender' => 'required|in:M,F',
      'dob' => 'required|Date',
      'bloodGroup' => 'max:4',
      'carerNameKh' => 'required|max:50',
      'relationship' => 'required|max:12',
      'provinceID' => 'required|numeric',
      'districtID' => 'required|numeric',
      'communeID' => 'required|numeric',
      'villageID' => 'required|numeric',
      'address' => 'max:200',
      'distance' => 'max:20',
      'phone1' => 'required|max:15',
      'phone2' => 'max:15',
      'isForeigner' => 'required|in:Y,N',
      'isEmployee' => 'required|in:Y,N',
      'employeeCardId' => 'max:10',
      'employeeCardExpiry' => 'max:10',
      'hasPoorId' => 'required|in:Y,N',
      'poorIdExpiry' => 'max:10',
      'hasHEF' => 'required|in:Y,N',
      'HEFExpiry' => 'max:10',
      'thirdPartyPayer' => 'required|in:Y,N',
      'insuranceName' => 'max:200',
      'billingCode' => 'required|max:2',
      'LMTR' => 'max:2',
   );

   protected $nationalities;

   public function __construct() {
      // Get the contents of the JSON file
      $this->nationalities = file_get_contents(public_path() . "/js/countries.json");
      // Convert to array 
      $this->nationalities = json_decode($this->nationalities, true);
   }


   /**
    * Retrieves all rows from table 'patients' that
    * are not marked as deleted.
    * 
    * @return patient
    */
   public function getPatients($pagesize, $pagenum, $condition) {
      // Call the stored procedure with output parameter.
      $patients = DB::select('CALL his.sp_getPatients(?, ?, ?, @numRows)', [$pagesize, $pagenum, $condition, 0]);

      // Get the out parameter returned by the Stored Procedure.
      // Such parameter is an integer value only when $condition is set.
      $numRows = DB::select('SELECT @numRows AS numRows');
      $data = array('patients'=>$patients, 'totRows'=>$numRows);
      return $data;
   }


   public function getNationalities() {
      return $this->nationalities;
   }


   /**
    * Saves the data into table 'patient'.
    * 
    * @param array $fields
    * @return void
    */
   public function store($fields) {
      if ($this->validate($fields)) {
         if (isset($fields['patientID'])) {
            $m = $this::find($fields['patientID']);
         }
         else {
            $m = new patient;
         }
         $m->firstName = $fields['firstName'];
         $m->lastName = $fields['lastName'];
         $m->phone = $fields['phone'];
         $m->email = $fields['email'];
         $m->dob = $fields['dob'];
         $m->save();
      }
   }


}
