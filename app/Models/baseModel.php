<?php
namespace App\Models;

use Illuminate\Support\Facades\DB;
use ReflectionClass;
use ReflectionProperty;

class baseModel
{
   const FIELD_TOO_LONG = 'The field content is too long.';
   const FIELD_REQUIRED = 'This field is required.';
   const FIELD_OUT_OF_RANGE = 'The value is out of range.';
   const INVALID_VALUE = 'The value provided is not valid.';
   const INVALID_EMAIL = 'The email address is not valid';

   /**
    * The table associated with the model.
    *
    * @property string $table
    */
   protected string $table;

   /**
    * The key column(s) of the model.
    *
    * @property string $primaryKey
    */
   protected string $primaryKey;

   /**
    * The properties of the class.
    * @property array $properties
    */
   protected array $properties;

   /**
    * The list of errors reported to the object.
    * @property array $errors
    */
   protected array $errors;

   
   public function __construct()
   {
      $obj = new ReflectionClass($this);
      $this->table = $obj->getShortName();

      // Store public and protected class properties in an array.
      $this->properties = $obj->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);

      // Keep only the name of the properties in the array.
      $this->properties = array_column($this->properties, 'name');
      $this->errors = [];
   }


   public function __set($name, $value)
   {
      // Check that the property name exists in the class.
      if (array_search($name, $this->properties) !== false) {
         $method = 'set'.ucfirst($name);
         if (method_exists($this, $method)) {
            $this->$method($value);
            return;
         }
      }
   }


   public function __get($propName)
   {
      // Check that the property name exists in the class.
      if (array_search($propName, $this->properties) !== false) {
         $method = 'get' . ucfirst($propName);
         if ( method_exists($this, $method) ) {
            return $this->$method();
         }
      }

      $trace = debug_backtrace();
      trigger_error(
         'Undefined property via __get(): ' . $propName .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
         E_USER_NOTICE
      );
      return null;
   } 


   /**
    * Returns the table name represented by the model.
    * @return string
    */
   protected function getTable(): string { return $this->table; }


   public function hasErrors() {
      return count($this->errors) > 0;
   }

   
   /**
    * Adds a new error message to the list of errors.
    * @param string $attribute
    * @param string $error
    * @return void
    */
   protected function addError(string $attribute, string $error = '' ) {
      array_push($this->errors, ['field'=>$attribute, 'message'=>$error]);
   }


   /**
    * Returns the list of errors reported by the model.
    * @return array
    */
   public function getErrors() {
      return $this->errors;
   }


   public function find(array $keyValues): array {
      $result = [];
      // Get table key columns.
      $query = "SHOW COLUMNS FROM ? WHERE `Key` = 'PRI'";
      $keyCols = DB::select($query, [$this->table]);

      if (count($keyCols) > 0) {
         // Build SQL statement
         $query = "SELECT * FROM ? WHERE ";
         $numCols = count($keyCols);
         for ($i=0; $i < $numCols; $i++) {
            $query .= ($i < $numCols) ? $keyCols[$i] . " = ? AND " : $keyCols[$i] . " = ?";
         }

         // Run the query
         $result = DB::select($query, $keyValues);
      }
      return $result;
   }


   /**
    * Receives a string containing a date, in whatever format, and 
    * returns a date formated as YYYY-MM-DD, if it is parsed successfully.
    * if not, it returns false.
    *
    * @param string $strDate
    * @return string
    */
   protected function dateToMySQL(string $strDate): string {
      $result = "Date format not recognized ($strDate)";
      $d = date_parse($strDate);
      if ($d['error_count'] == 0) {
         $result = date("Y-m-d", strtotime($d['year'].'-'.$d['month'].'-'.$d['day']));
      }
      return $result;
   }

}
